commit 0e3dfb9f705ca78be34cd70fd59d67c431e29cc7
Author: Takashi Iwai <tiwai@suse.de>
Date:   Sat Sep 9 17:42:03 2023 +0200

    pcm: Fix segfault with 32bit libs
    
    The recent rearrangement of header inclusion order caused a regression
    showing segfaults on 32bit Arm.  The primary reason is the
    inconsistent compile condition depending on the inclusion of config.h;
    while most of other code include pcm_local.h (that implicitly includes
    config.h) at first, pcm_direct.c doesn't do it, hence the access with
    direct plugins crashes.
    
    For fixing it, we need to include config.h at the beginning.  But,
    it's better to include pcm_local.h for all relevant code for
    consistency.  The patch does it, and also it adds the guard in
    pcm_local.h for double inclusions.
    
    Fixes: ad3a8b8b314e ("reshuffle included files to include config.h as first")
    Link: https://github.com/alsa-project/alsa-lib/issues/352
    Signed-off-by: Takashi Iwai <tiwai@suse.de>

diff --git a/src/pcm/pcm_direct.c b/src/pcm/pcm_direct.c
index 040fc160..e53e5923 100644
--- a/src/pcm/pcm_direct.c
+++ b/src/pcm/pcm_direct.c
@@ -19,6 +19,7 @@
  *
  */
   
+#include "pcm_local.h"
 #include <stdio.h>
 #include <stdlib.h>
 #include <stddef.h>
diff --git a/src/pcm/pcm_dmix.c b/src/pcm/pcm_dmix.c
index 7cd3c508..55cae3e7 100644
--- a/src/pcm/pcm_dmix.c
+++ b/src/pcm/pcm_dmix.c
@@ -26,7 +26,7 @@
  *
  */
   
-#include "config.h"
+#include "pcm_local.h"
 #include <stdio.h>
 #include <stdlib.h>
 #include <stddef.h>
diff --git a/src/pcm/pcm_dshare.c b/src/pcm/pcm_dshare.c
index 454b39a9..c0329098 100644
--- a/src/pcm/pcm_dshare.c
+++ b/src/pcm/pcm_dshare.c
@@ -26,6 +26,7 @@
  *
  */
   
+#include "pcm_local.h"
 #include <stdio.h>
 #include <stdlib.h>
 #include <stddef.h>
diff --git a/src/pcm/pcm_dsnoop.c b/src/pcm/pcm_dsnoop.c
index d3ce300c..bf67c68a 100644
--- a/src/pcm/pcm_dsnoop.c
+++ b/src/pcm/pcm_dsnoop.c
@@ -26,6 +26,7 @@
  *
  */
   
+#include "pcm_local.h"
 #include <stdio.h>
 #include <stdlib.h>
 #include <stddef.h>
diff --git a/src/pcm/pcm_local.h b/src/pcm/pcm_local.h
index 6a0e71e7..152c92c3 100644
--- a/src/pcm/pcm_local.h
+++ b/src/pcm/pcm_local.h
@@ -20,6 +20,9 @@
  *
  */
 
+#ifndef __PCM_LOCAL_H
+#define __PCM_LOCAL_H
+
 #include "config.h"
 
 #include <stdio.h>
@@ -1223,3 +1226,5 @@ static inline void snd_pcm_unlock(snd_pcm_t *pcm)
 #define snd_pcm_lock(pcm)		do {} while (0)
 #define snd_pcm_unlock(pcm)		do {} while (0)
 #endif /* THREAD_SAFE_API */
+
+#endif /* __PCM_LOCAL_H */
diff --git a/src/pcm/pcm_shm.c b/src/pcm/pcm_shm.c
index f0bfd934..d9596547 100644
--- a/src/pcm/pcm_shm.c
+++ b/src/pcm/pcm_shm.c
@@ -26,6 +26,7 @@
  *
  */
   
+#include "pcm_local.h"
 #include <stdio.h>
 #include <stdlib.h>
 #include <stddef.h>
commit 81a7a93636d9472fcb0c2ff32d9bfdf6ed10763d
Author: Jaroslav Kysela <perex@perex.cz>
Date:   Wed Sep 13 12:27:21 2023 +0200

    reshuffle included files to include config.h as first - v2
    
    config.h may contain defines like _FILE_OFFSET_BITS which influence
    the system wide include files (off_t types, open -> open64 function
    usage etc.).
    
    Fixes: ad3a8b8b ("reshuffle included files to include config.h as first")
    Related: https://github.com/alsa-project/alsa-lib/pull/333
    Signed-off-by: Jaroslav Kysela <perex@perex.cz>

diff --git a/src/control/setup.c b/src/control/setup.c
index 88635e42..fb096117 100644
--- a/src/control/setup.c
+++ b/src/control/setup.c
@@ -29,13 +29,13 @@
  *
  */
 
+#include "local.h"
 #include <stdio.h>
 #include <stdlib.h>
 #include <stdarg.h>
 #include <unistd.h>
 #include <string.h>
 #include <ctype.h>
-#include "local.h"
 
 #ifndef DOC_HIDDEN
 typedef struct {
diff --git a/src/rawmidi/rawmidi.c b/src/rawmidi/rawmidi.c
index 316f524b..c4b45fa2 100644
--- a/src/rawmidi/rawmidi.c
+++ b/src/rawmidi/rawmidi.c
@@ -144,12 +144,12 @@ This example shows open and read/write rawmidi operations.
  * Shows open and read/write rawmidi operations.
  */
  
+#include "rawmidi_local.h"
 #include <stdio.h>
 #include <stdlib.h>
 #include <stdarg.h>
 #include <unistd.h>
 #include <string.h>
-#include "rawmidi_local.h"
 
 /**
  * \brief setup the default parameters
diff --git a/src/rawmidi/rawmidi_local.h b/src/rawmidi/rawmidi_local.h
index 19dbf725..f0bb06a7 100644
--- a/src/rawmidi/rawmidi_local.h
+++ b/src/rawmidi/rawmidi_local.h
@@ -19,10 +19,10 @@
  *
  */
 
+#include "local.h"
 #include <stdio.h>
 #include <stdlib.h>
 #include <limits.h>
-#include "local.h"
 
 typedef struct {
 	int (*close)(snd_rawmidi_t *rawmidi);
diff --git a/src/rawmidi/rawmidi_virt.c b/src/rawmidi/rawmidi_virt.c
index 884b8ff8..04c485d3 100644
--- a/src/rawmidi/rawmidi_virt.c
+++ b/src/rawmidi/rawmidi_virt.c
@@ -19,13 +19,11 @@
  *
  */
 
-#include <stdio.h>
-#include <stdlib.h>
+#include "rawmidi_local.h"
 #include <unistd.h>
 #include <string.h>
 #include <fcntl.h>
 #include <sys/ioctl.h>
-#include "rawmidi_local.h"
 #include "seq.h"
 #include "seq_midi_event.h"
 
diff --git a/src/rawmidi/ump.c b/src/rawmidi/ump.c
index 25fbaff2..39c1c4a9 100644
--- a/src/rawmidi/ump.c
+++ b/src/rawmidi/ump.c
@@ -4,10 +4,6 @@
  * \brief Universal MIDI Protocol (UMP) Interface
  */
 
-#include <stdio.h>
-#include <stdlib.h>
-#include <limits.h>
-#include "local.h"
 #include "rawmidi_local.h"
 #include "ump_local.h"
 
diff --git a/src/seq/seq.c b/src/seq/seq.c
index 899dfe9f..fd8ca30e 100644
--- a/src/seq/seq.c
+++ b/src/seq/seq.c
@@ -777,8 +777,8 @@ void event_filter(snd_seq_t *seq, snd_seq_event_t *ev)
 
 */
 
-#include <poll.h>
 #include "seq_local.h"
+#include <poll.h>
 
 /****************************************************************************
  *                                                                          *
diff --git a/src/seq/seq_hw.c b/src/seq/seq_hw.c
index a51ebfb6..b74948c8 100644
--- a/src/seq/seq_hw.c
+++ b/src/seq/seq_hw.c
@@ -20,9 +20,9 @@
  *
  */
 
+#include "seq_local.h"
 #include <fcntl.h>
 #include <sys/ioctl.h>
-#include "seq_local.h"
 
 #ifndef PIC
 /* entry for static linking */
diff --git a/src/seq/seq_local.h b/src/seq/seq_local.h
index 9b4a6545..46824806 100644
--- a/src/seq/seq_local.h
+++ b/src/seq/seq_local.h
@@ -23,10 +23,10 @@
 #ifndef __SEQ_LOCAL_H
 #define __SEQ_LOCAL_H
 
+#include "local.h"
 #include <stdio.h>
 #include <stdlib.h>
 #include <limits.h>
-#include "local.h"
 
 #define SND_SEQ_OBUF_SIZE	(16*1024)	/* default size */
 #define SND_SEQ_IBUF_SIZE	500		/* in event_size aligned */
diff --git a/src/seq/seq_midi_event.c b/src/seq/seq_midi_event.c
index df09bde3..95a44e9b 100644
--- a/src/seq/seq_midi_event.c
+++ b/src/seq/seq_midi_event.c
@@ -28,10 +28,10 @@
  *   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
  */
 
+#include "local.h"
 #if HAVE_MALLOC_H
 #include <malloc.h>
 #endif
-#include "local.h"
 
 #ifndef DOC_HIDDEN
 
diff --git a/src/seq/seqmid.c b/src/seq/seqmid.c
index 55651f38..9ec93ee8 100644
--- a/src/seq/seqmid.c
+++ b/src/seq/seqmid.c
@@ -20,14 +20,12 @@
  *
  */
 
-#include <stdio.h>
-#include <stdlib.h>
+#include "seq_local.h"
 #include <unistd.h>
 #include <string.h>
 #include <fcntl.h>
 #include <ctype.h>
 #include <sys/ioctl.h>
-#include "seq_local.h"
 
 /**
  * \brief queue controls - start/stop/continue
diff --git a/src/userfile.c b/src/userfile.c
index 4a740834..492ea9cb 100644
--- a/src/userfile.c
+++ b/src/userfile.c
@@ -18,7 +18,7 @@
  *
  */
   
-#include <config.h>
+#include "config.h"
 #include <string.h>
 #include <errno.h>
 #include <assert.h>
