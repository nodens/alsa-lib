Description: seq: Clear UMP event flag for legacy apps
 It seems that some applications (at least Chrome WebMIDI) set random
 bits to the flags of event packet, and this confuses as if they were
 UMP-events, which are eventually filtered out.

 Although it's a bug of applications, it's better to avoid the
 regressions.  So this patch forcibly clears the UMP flag of the
 incoming and outgoing events when the application is running in the
 legacy mode (i.e. midi_version = 0).
Author: Takashi Iwai <tiwai@suse.de>
Origin: upstream, https://github.com/alsa-project/alsa-lib/commit/2fca03e792ef1b740e8a7370fdd360d0b627c84c
Bug: https://github.com/alsa-project/alsa-lib/issues/360
Applied-Upstream: https://github.com/alsa-project/alsa-lib/commit/2fca03e792ef1b740e8a7370fdd360d0b627c84c
Last-Update: 2023-11-10
---
This patch header follows DEP-3: http://dep.debian.net/deps/dep3/
--- a/src/seq/seq.c
+++ b/src/seq/seq.c
@@ -4161,6 +4161,13 @@
 	return result;
 }

+/* workaround for broken legacy apps that set UMP event bit unexpectedly */
+static void clear_ump_for_legacy_apps(snd_seq_t *seq, snd_seq_event_t *ev)
+{
+	if (!seq->midi_version && snd_seq_ev_is_ump(ev))
+		ev->flags &= ~SNDRV_SEQ_EVENT_UMP;
+}
+
 /**
  * \brief output an event onto the lib buffer without draining buffer
  * \param seq sequencer handle
@@ -4178,6 +4185,7 @@
 {
 	int len;
 	assert(seq && ev);
+	clear_ump_for_legacy_apps(seq, ev);
 	len = snd_seq_event_length(ev);
 	if (len < 0)
 		return -EINVAL;
@@ -4238,6 +4246,7 @@
 	ssize_t len;
 	void *buf;

+	clear_ump_for_legacy_apps(seq, ev);
 	len = snd_seq_event_length(ev);
 	if (len < 0)
 		return len;
@@ -4374,6 +4383,7 @@
 	snd_seq_event_t *ev;

 	*retp = ev = (snd_seq_event_t *)(seq->ibuf + seq->ibufptr * packet_size);
+	clear_ump_for_legacy_apps(seq, ev);
 	seq->ibufptr++;
 	seq->ibuflen--;
 	if (! snd_seq_ev_is_variable(ev))
